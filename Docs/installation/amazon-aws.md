# Crear un clúster de Kubernetes desde AWS
##Prerrequisitos
1. Una cuenta de Amazon Web Services.
1. KOPS, para crear un cluster de Kubernetes mediante las utilidades de una cuenta amazon
##Instalación:
* Descargar AWS CLI: se puede realizar mediante este enlace: https://docs.aws.amazon.com/es_es/cli/latest/userguide/installing.html
* Instalar KOPS: https://github.com/kubernetes/kops.git
* Crear un usuario/Asignar un usuario de AWS (Amazon Web Services): Los permisos que se han de asignar a este usuario serán:
```bash
AmazonEC2FullAccess
IAMFullAccess
AmazonS3FullAccess
AmazonVPCFullAccess
AmazonRoute53FullAccess
```

* configuraremos el CLI con nuestro usuario:
```bash
 aws configure
``` 

* Crearemos un bucket en amazon para guardar la configuración del cluster
```bash
 bucket_name=${nombre_empresa}-kops-state-store
 aws s3api create-bucket \
 --bucket ${bucket_name} \
 --region [amazon-region-name]
# si está fuera de los EE.UU. hay que agregar un locationconstraint
```
donde nombre_empresa es el nombre ´único del amazon S3 Bucket.
donde `[amazon-region-name]` es el nombre de la región de amazon.
* Actualizamos el bucket para que tenga versionado:
```bash
  aws s3api put-bucket-versioning --bucket ${bucket_name} --versioning-configuration Status=Enabled
```
* Exportamos estas dos variables para indicar el dominio del cluster Kubernetes y la URL del bucket de S3
```bash
export KOPS_CLUSTER_NAME=imesh.k8s.local
export KOPS_STATE_STORE=s3://${bucket_name}
```
podemos agregarlo a nuestro .profile de nuestro shell
*  Utilizar kops create para crear un clúster de kubernetes usando herramientas de AWS
```bash
kops create cluster \
--node-count=3 \
--node-size=c5.large \
--zones=[amazon-region-name] \
--name=${KOPS_CLUSTER_NAME}
```
si surgen problemas de autenticación, se pueden exportar las siguientes variables:
```bash
export AWS_ACCESS_KEY=AccessKeyValue
export AWS_SECRET_KEY=SecretAccessKeyValue
```
* Comprobaremos que el clúster se ha creado bien mediante la siguiente orden:
```bash
 kops edit cluster --name ${KOPS_CLUSTER_NAME}
```
Este comando nos llevará a un editor en el que podemos editar el archivo YAML del clúster.
Una vez hemos escrito el YAML de nuestro clúster actualizaremos el mismo con el siguiente comando:
```bash
kops update cluster --name ${KOPS_CLUSTER_NAME} --yes
```
* Validamos que el clúster funciona correctamente:
```bash
kops validate cluster
```
* Ahora instalaremos el Dashboard: 
```bash
$ kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```
y accederemos al dashboard de esta manera:
```
https://<master-ip>:<apiserver-port>/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
```

con el usuario admin y la password de éste se consigue mediante el comando: 
```bash
 $ kops get secrets kube --type secret -oplaintext
```
donde ``<master-ip>:<piserver-port>`` se consigue mediante el comando `kubectl cluster-info`.
El sistema nos pedirá un acceso mediante Token o mediante KubeConfig, pero nosotros tomaremos la opción de RBAC mediante el siguiente archivo dashboard.yaml
```bash
#meter el YAML a continuación
$ vi dashboard.yaml
```

```YAML
#dashboard.yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system
```
y lo aplicaremos al clúster mediante la orden:
```bash
 $ kubectl apply -f ./dashboard.yaml
```
donde dashboard.yaml es el archivo que creamos en el paso anterior
de esta manera ya podemos acceder al kubernetes dashboard