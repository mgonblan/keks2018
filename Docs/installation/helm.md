# instalación de helm (package manager para clusters de Kubernetes)
## Requisitos:
   * Un cluster de Kubernetes funcionando, (Puede ser Amazon, Google, IBM u otro).
   * Decidir qué seguridad se aplicará (en nuestro caso ServiceAccount).
## Instalar helm:
Para instalar helm, lo más sencillo es descargar el binario en función del sistema desde https://gitlab.com/mgonblan/keks2018.git. 
* desempaquetar el archivo mediante 
```bash 
   $ tar -xvf [archivo-helm].tar.gz
   $ mv linux-amd64/helm /usr/local/bin/helm
```
* Realizar el siguiente comando para crear un service-account para tiller:
    ```bash 
    $ kubectl apply -f tiller-clusterrolebinding.yaml
    ```
* inicializar helm mediante el siguiente comando:
  ```bash 
     $ helm init --service-account=tiller
  ```
## instalación del paquete del etiquetador
Este paquete consiste en la instalación de un ecosistema con Kafka, Elasticsearch y Kibana. Spark viene aparte con el comando spark submit.
* Instalación:
  1. Descargar el repositorio mediante: 
  ```bash 
     $ git clone https://gitlab.com/mgonblan/keks2018.git
  ```
  2. Una vez se ha clonado el repositorio, exportamos la variable del directorio del repositorio: 
      - Debemos actualizar las dependencias del paquete con los repositorios mediante este comando:
```bash
    export KEKS_HOME=[directorio donde hemos descargado el repo]/keks2018/helm-charts/KEKSPARK
    $ helm repo update
    $ cd $KEKS_HOME
    $ helm dep up
```    
  3. Realizamos: 
  ```bash 
     $ helm template $KEKS_HOME
  ``` 
  para que nos saque todos los objetos traducidos a kubernetes. 
  
  4. instalaremos o actualizaremos la release mediante este comando:
     ```bash 
        $ nombre_release=[trabajo]
        $ helm upgrade ${nombre_release} --install --set kibana.env.ELASTICSEARCH_URL=http://${nombre_release}-elasticsearch-discovery.default.svc.cluster.local:9200 $KEKS_HOME
     ``` 
## Actualizar la distribución
  
  a veces, queremos afinar nuestra distribución con otros valores, pues queremos modificar el clúster para exponer o no nuestros servicios. Para ello usaremos el comando:
  
```bash
  $ helm upgrade ${nombre_release} [--set ([clave=valor],)] (-f archivo.yaml)
```

## Borrar todo
* Borrar el deployment con helm
Es tan simple como hacer `helm delete ${nombre_distribución} --purge` y `kubectl delete pvc --all`
* eliminar el clúster de kubernetes:
En función de la plataforma hay un procedimiento.
