# Configuración de Spark para que funcione con kubernetes
## * Prerrequisitos:
	- Un cluster de kubernetes.
	- Una instalación de Spark.
## * Procedimiento:
  1. [Opcional] Cración de la imagen de Docker.
      con esta opción construiremos una imagen docker para guardar en el registro. Para ello haremos el siguiente comando.
      ```bash
         $ ./bin/docker-image-tool.sh -r <repo> -t my-tag build
         $ ./bin/docker-image-tool.sh -r <repo> -t my-tag push
      ```
  2. Configuración RBAC del clúster.
     Este paso es muy importante, pues lo necesitamos para que funcione el comando spark-submit.
     1. Comprobaremos clusterrolebinding mediante el comando:
     ```bash
        $ kubectl get serviceaccount
        $ kubectl get clusterrolebinding
     ```
     2. Crearemos el clusterrolebinding & serviceaccount mediante estos comandos:
     ```bash
        $ kubectl create serviceaccount spark
        
        $ kubectl create clusterrolebinding spark-role --clusterrole=edit --serviceaccount=default:spark --namespace=default

        $ kubectl get serviceaccount (para conocer los servicesaccount)
     ```
     
  3. Probar el comando spark-submit.
  ejecutaremos el comando spark-submit con el serviceaccount recién creado.
  ```bash
     $ bin/spark-submit \
    --master k8s://[ip del cluster] \
    --deploy-mode cluster \
    --name spark-wordcount \
    --class org.apache.spark.examples.JavaWordCount \
    --conf spark.executor.instances=5 \
    --conf spark.kubernetes.authenticate.driver.serviceAccountName=spark  \
    --conf spark.kubernetes.container.image=registry.eu-de.bluemix.net/ampbd/spark:2.3.1 \
    local:////usr/local/Cellar/apache-spark/2.3.1/libexec/examples/jars/spark-examples_2.11-2.3.1.jar gs://etiquetador-textos/quijote.txt (por ejemplo) 
  ```
  acceder a  
  ```bash
   $ kubectl port-forward <driver-pod-name> 4040:4040
  ```
nota: Hasta la versión 2.4 no se integrará pyspark en kubernetes
  